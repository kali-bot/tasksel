# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: tasksel\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-25 11:52+0100\n"
"PO-Revision-Date: 2006-10-03 09:34+0300\n"
"Last-Translator: Siim Põder <windo@windowlicker.dyn.ee>\n"
"Language-Team: Debiani installeri eestindus <debian-boot-et@linux.ee>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Estonian\n"
"X-Poedit-Country: ESTONIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../tasksel.pl:418
msgid ""
"Usage:\n"
"tasksel install <task>...\n"
"tasksel remove <task>...\n"
"tasksel [options]\n"
"\t-t, --test          test mode; don't really do anything\n"
"\t    --new-install   automatically install some tasks\n"
"\t    --list-tasks    list tasks that would be displayed and exit\n"
"\t    --task-packages list available packages in a task\n"
"\t    --task-desc     returns the description of a task\n"
msgstr ""
"Kasutus:\n"
"tasksel install <ülesanne>...\n"
"tasksel remove <ülesanne>...\n"
"tasksel [valikud]\n"
"\t-t, --test          testirežiim; ära tegelikult miskit tee\n"
"\t    --new-install   paigalda mõned ülesanded automaatselt\n"
"\t    --list-tasks    loetle väljastatavad ülesanded ning välju\n"
"\t    --task-packages loetle ülesande saadaval pakid\n"
"\t    --task-desc     tagastab ülesande kirjelduse\n"

#: ../tasksel.pl:659
msgid "apt-get failed"
msgstr "apt-get luhtus"
