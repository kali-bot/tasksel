# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-31 17:50-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Description
#: ../po/debian-tasks.desc:1001
msgid "Kali core packages"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:2001
msgid "Desktop environment [selecting this item has no effect]"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:2001
msgid "This task is an heading grouping the various desktop environments."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:3001
msgid "GNOME"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:3001
msgid "This task installs the GNOME desktop environment."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:4001
msgid "KDE Plasma"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:4001
msgid "This task installs the KDE Plasma desktop environment."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:5001
msgid "LXDE"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:5001
msgid "This task installs the LXDE desktop environment."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:6001
msgid "MATE"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:6001
msgid "This task installs the MATE desktop environment."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:7001
msgid "Xfce (Kali's default desktop environment)"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:7001
msgid "This task installs kali-desktop-xfce."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:8001
msgid "Collection of tools [selecting this item has no effect]"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:8001
msgid "This task is an heading grouping generic metapackages."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:9001
msgid "default -- recommended tools (available in the live system)"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:9001
msgid "This task installs kali-linux-default."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:10001
msgid ""
"everything -- almost all tools [>= 7 GB to download, >= 16 GB installed]"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:10001
msgid "This task installs kali-linux-everything."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:11001
msgid "large -- default selection plus additional tools"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:11001
msgid "This task installs kali-linux-large."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:12001
msgid "top10 -- the 10 most popular tools"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:12001
msgid "This task installs kali-tools-top10 and kali-linux-core."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:13001
msgid "standard system utilities"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:13001
msgid ""
"This task sets up a basic user environment, providing a reasonably small "
"selection of services and tools usable on the command line."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:14001
msgid "Install tools by purpose [selecting this item has no effect]"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:14001
msgid "This task is a heading grouping kali-tools-* metapackages."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:15001
msgid "Database assessment"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:15001
msgid "This task installs kali-tools-database."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:16001
msgid "Exploitation"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:16001
msgid "This task installs kali-tools-exploitation."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:17001
msgid "Forensics"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:17001
msgid "This task installs kali-tools-forensics."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:18001
msgid "Information gathering"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:18001
msgid "This task installs kali-tools-information-gathering."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:19001
msgid "Password cracking"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:19001
msgid "This task installs kali-tools-passwords."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:20001
msgid "Post exploitation"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:20001
msgid "This task installs kali-tools-post-exploitation."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:21001
msgid "Reporting"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:21001
msgid "This task installs kali-tools-reporting."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:22001
msgid "Reverse engineering"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:22001
msgid "This task installs kali-tools-reverse-engineering."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:23001
msgid "Sniffing and spoofing"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:23001
msgid "This task installs kali-tools-sniffing-spoofing."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:24001
msgid "Social engineering"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:24001
msgid "This task installs kali-tools-social-engineering."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:25001
msgid "Vulnerability analysis"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:25001
msgid "This task installs kali-tools-vulnerability."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:26001
msgid "Web applications assessment"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:26001
msgid "This task installs kali-tools-web."
msgstr ""

#. Description
#: ../po/debian-tasks.desc:27001
msgid "Wireless"
msgstr ""

#. Description
#: ../po/debian-tasks.desc:27001
msgid "This task installs kali-tools-wireless."
msgstr ""
